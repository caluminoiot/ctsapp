import app
import os

os.environ['DBUS_SESSION_BUS_ADDRESS'] = "unix:path=/run/dbus/system_bus_socket"
os.environ["DISPLAY"] = ":0"
app.launch()
